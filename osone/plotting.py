import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


def plot_max_ozone_height(worldmap_data, plotpath=""):
    """Plots the height of the maxmimum level of ozone on a world map.

    Args:

        worldmap_data: Xarray dataarray containing the altitudes of maximum
        ozone concentration.

        plotpath: If string is not empty, save figure to this directory.
        If empty, just show the figure.

    """
    plt.figure(1)
    ax = plt.axes(projection=ccrs.EqualEarth())
    worldmap_data.plot(ax=ax, transform=ccrs.PlateCarree())
    ax.coastlines()
    ax.gridlines()
    plt.title("Altitude [m] of the maximum level of ozone in the athmosphere", y=1.5)

    if plotpath is None:
        plt.show()
    else:
        plotfile = os.path.join(plotpath, "global_map" + ".png")
        plt.savefig(plotfile)


def plot_mean_ozone_lats(ds_global, ds_tropics, ds_mid_lats, ds_high_lats, plotpath=""):
    """Plots vertical profiles of ozone values for different latitudes.

    Args:

        ds_global: Xarray dataset containing ozone values globally.
        ds_tropics: Xarray dataset containing ozone values for the tropics.
        ds_mid_lats: Xarray dataset containing ozone values for mid-lats.
        ds_high_lats: Xarray dataset containing ozone values for high-lats.

        plotpath: If string is not empty, save figure to this directory.
        If empty, just show the figure.

    """
    plt.figure(2)
    plt.plot(ds_global.ozone.mean("time"), ds_global.ozone.altitude, label="global")
    plt.plot(ds_tropics.ozone.mean("time"), ds_tropics.ozone.altitude, label="tropics")
    plt.plot(
        ds_mid_lats.ozone.mean("time"),
        ds_mid_lats.ozone.altitude,
        label="mid-latitudes",
    )
    plt.plot(
        ds_high_lats.ozone.mean("time"),
        ds_high_lats.ozone.altitude,
        label="high-latitudes",
    )
    plt.title("Vertical profiles of ozone values in different altitudes")
    plt.xlabel("ozone [kg kg-1]")
    plt.legend()

    if plotpath is None:
        plt.show()
    else:
        plotfile = os.path.join(plotpath, "vertical_profiles" + ".png")
        plt.savefig(plotfile)


def plot_ozonehole_min(data_ozonehole_ls, plotpath=""):
    """Plots timeseries data of ozonevalues from the ozonehole over antarctica.

    Args:

        data_ozonehole_ls: Xarray dataset containing timeseries data of the
        season of lowest ozone concentration to be plotted.

        plotpath: If string is not empty, save figure to this directory.
        If empty, just show the figure.

    """
    plt.figure(3)
    data_ozonehole_ls.ozone.mean(["longitude_bins", "latitude_bins", "altitude"]).plot()
    plt.ylabel("ozone [kg kg-1]")
    plt.title("Changes in the ozone hole over antarctica in the months: JJA")

    if plotpath is None:
        plt.show()
    else:
        plotfile = os.path.join(plotpath, "ozonehole_timeseries" + ".png")
        plt.savefig(plotfile)
