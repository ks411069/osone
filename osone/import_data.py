import xarray as xr


def read_ozone(datapath, startdate, enddate):
    """Reads data from netCDF-file via xarray.

    Args:

        datapath: Path to the file containing the netCDF data.

        startdate: Function selects data from this date onwards.

        enddate: Function selects data only until this date.

    Returns:

        Xarray dataset containing the data.

    """
    with xr.open_dataset(datapath) as ds_ozone:
        ds_ozone.load()

    if startdate is not None and enddate is not None:
        ds_ozone = ds_ozone.sel(time=slice(startdate, enddate))

    return ds_ozone
