import argparse
import os

import pandas as pd

from . import import_data, computing, plotting


def osone():
    """ Entry point."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--datapath",
        required=True,
        type=str,
        help="Enter the path leading to the file of the input data.",
    )
    parser.add_argument(
        "--plotpath",
        required=False,
        type=str,
        help="Do you want to save the computed plots into a file? Enter a path!",
    )
    parser.add_argument(
        "--startdate",
        required=False,
        type=pd.Timestamp.fromisoformat,
        help="Start date of data used (e.g. 1991-01-01)",
    )
    parser.add_argument(
        "--enddate",
        required=False,
        type=pd.Timestamp.fromisoformat,
        help="End date of data used (e.g. 2021-12-01)",
    )
    parser.add_argument(
        "--plot_maxozone",
        required=False,
        action="store_true",
        help="Do you want to plot a global map of the height of the maximum\
            level of ozone?",
    )
    parser.add_argument(
        "--plot_meanozone",
        required=False,
        action="store_true",
        help="Do you want to plot vertical profiles of the mean ozone\
            concentrations for different latitudes?",
    )
    parser.add_argument(
        "--plot_ozonehole",
        required=False,
        action="store_true",
        help="Do you want to plot a time series for the ozone hole\
            over antarctica in the season of lowest ozone concentration?",
    )

    args = parser.parse_args()

    if os.path.exists(args.datapath) is False:
        parser.error("The path to your datafile does not exist.")

    if args.plotpath and os.path.exists(args.plotpath) is False:
        parser.error("The plotpath you entered does not exist.")

    if (
        args.startdate is not None
        and args.enddate is None
        or args.startdate is None
        and args.enddate is not None
    ):
        parser.error(
            "You entered either only a start- or enddate.Enter both\
                     or none!"
        )

    if args.plot_maxozone:
        maxozone = computing.max_ozone_height(
            import_data.read_ozone(args.datapath, args.startdate, args.enddate)
        )
        plotting.plot_max_ozone_height(maxozone, plotpath=args.plotpath)
    if args.plot_meanozone:
        ds_global = computing.mean_ozone_global(
            import_data.read_ozone(args.datapath, args.startdate, args.enddate)
        )
        ds_tropics = computing.mean_ozone_tropics(
            import_data.read_ozone(args.datapath, args.startdate, args.enddate)
        )
        ds_mid_lats = computing.mean_ozone_mid_lats(
            import_data.read_ozone(args.datapath, args.startdate, args.enddate)
        )
        ds_high_lats = computing.mean_ozone_high_lats(
            import_data.read_ozone(args.datapath, args.startdate, args.enddate)
        )
        plotting.plot_mean_ozone_lats(
            ds_global, ds_tropics, ds_mid_lats, ds_high_lats, plotpath=args.plotpath
        )
    if args.plot_ozonehole:
        ozonehole = computing.ozonehole_min(
            import_data.read_ozone(args.datapath, args.startdate, args.enddate)
        )
        plotting.plot_ozonehole_min(ozonehole, plotpath=args.plotpath)
