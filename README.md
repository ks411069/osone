# osone

A project using netCDF data of ozone values in order to compute and plot things with xarray.

This python package uses data of ozone values from different latitudes, longitudes, altitudes 
and over a timespan. It uses xarray in order to calculate the altitude of maximum ozone levels
around the world and plots it using cartopy. It computes and plots vertical profiles of mean
ozone values and computes and plots a timeseries of the ozonehole over antarctica.

The ozone data can be downloaded here: https://cloud.uni-graz.at/s/kJSFWiibLJfyyBZ

## Installation

Create a new virtual environment, e.g., wiht mamba:
mamba create --name mynewenv python
mamba activate mynewenv

Use pip ("python -m pip install .") in your virtual environment
to install the project.

## Usage

1. Enter a path to your netcdf ozone datafile. 
2. Optional: Enter a path, where you want to save the generated plotfiles.
3. Optional: Enter a start- and end date.
4. Specify which functions should be executed.

For usage information, type `osone --help`.