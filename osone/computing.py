import xarray as xr
import numpy as np


def max_ozone_height(ds_ozone):
    """Selects the altitude for the maxmimum ozone levels in a dataset.

    Args:

        ds: Xarray dataset containing netCDF data of ozone values.

    Returns:

        worldmap_data: Xarray dataarray containing the altitudes of maximum
        ozone concentration.

    """
    mean_time = ds_ozone.mean("time")
    height_max_ozone = xr.where(
        mean_time["ozone"] == mean_time["ozone"].max(dim=["altitude"]),
        mean_time["altitude"],
        np.nan,
    )
    worldmap_data = height_max_ozone.sum(dim="altitude").transpose()

    return worldmap_data


def mean_ozone_global(ds_ozone):
    """Selects mean ozone values (mean of latitude and longitude) globally.

    Args:

        ds: Xarray dataset containing netCDF data of ozone values.

    Returns:

        ds_global: Xarray dataset containing ozone values globally.

    """
    ds_global = ds_ozone.mean(["latitude_bins", "longitude_bins"])

    return ds_global


def mean_ozone_tropics(ds_ozone):
    """Selects mean ozone values (mean of latitude and longitude) for the
    tropics.

    Args:

        ds: Xarray dataset containing netCDF data of ozone values.

    Returns:

        ds_tropics: Xarray dataset containing ozone values for the tropics.

    """

    # Slices according to an approximation of latitudes for tropics.
    ds_tropics = ds_ozone.sel(latitude_bins=slice(-25, 25)).mean(
        ["latitude_bins", "longitude_bins"]
    )

    return ds_tropics


def mean_ozone_mid_lats(ds_ozone):
    """Selects mean ozone values (mean of latitude and longitude) for the
    mid-latitudes.

    Args:

        ds: Xarray dataset containing netCDF data of ozone values.

    Returns:

        ds_mid_lats: Xarray dataset containing ozone values for mid-lats.

    """

    # Selects according to an approximation of latitudes for the mid-lats.
    ds_mid_lats = ds_ozone.sel(latitude_bins=[-55, -45, -35, 35, 45, 55]).mean(
        ["latitude_bins", "longitude_bins"]
    )

    return ds_mid_lats


def mean_ozone_high_lats(ds_ozone):
    """Selects mean ozone values (mean of latitude and longitude) for the
    high-latitudes.

    Args:

        ds: Xarray dataset containing netCDF data of ozone values.

    Returns:

        ds_high_lats: Xarray dataset containing ozone values for high-lats.

    """
    # Selects according to an approximation of latitudes for the high-lats.
    ds_high_lats = ds_ozone.sel(latitude_bins=[-85, -75, -65, 65, 75, 85]).mean(
        ["latitude_bins", "longitude_bins"]
    )

    return ds_high_lats


def ozonehole_min(ds_ozone):
    """Computes the season of lowest ozone concentration over the ozonehole in
    Antarctica and selects the data accordingly.

    Args:

        ds: Xarray dataset containing netCDF data of ozone values.

    Returns:

        data_ozonehole_ls: Xarray dataset containing ozonevalues of the
        ozonehole over antarctica during the season of its lowest concentration.

    """
    # Slicing the dataset so its only antarctica (latitude ~ -85 until -65) and
    # only the stratosphere/ozone layer (altitude ~ 7000m until 50000m)
    antarctica = ds_ozone.sel(
        altitude=slice(7000, 50000), latitude_bins=slice(-85, -65)
    )

    mean_seasons = antarctica.groupby("time.season").mean(
        ["latitude_bins", "longitude_bins", "altitude", "time"]
    )
    low_season = mean_seasons.ozone.idxmin("season")
    data_low_season = antarctica.sel(time=antarctica.time.dt.season == low_season)
    data_ozonehole_ls = data_low_season.groupby(data_low_season.time.dt.year).mean(
        "time"
    )

    return data_ozonehole_ls
